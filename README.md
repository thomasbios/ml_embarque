# ML Embarqué

## Authors

* thomas.alia
* reda.sahrane
* simon.llagone

## How to start the program

### Start zookeper and kafka and our two consummers

`./build/start_consumer.sh`

Go to `localhost:8000` for the front end

### Start a producer

`./build/start_producer.sh`
