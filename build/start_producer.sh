#! /bin/sh

# This script must be run from .
# The consumer docker compose must be running

if [ "$1" != "--skipbuild" ]; then
    docker build -t ml_embarque_producer -f ./build/dockerfiles/producer.Dockerfile . || exit $?
fi

docker run --rm \
            --network="host" \
            --name ml_embarque_producer$RANDOM \
            ml_embarque_producer
