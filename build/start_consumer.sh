#! /bin/sh

# This script must be run from .
if [ "$1" != "--skipbuild" ]; then
    docker build -t ml_embarque_front -f ./build/dockerfiles/front.Dockerfile . || exit $?
    docker build -t ml_embarque_consumer -f ./build/dockerfiles/consumer.Dockerfile . || exit $?
fi

cd build && docker-compose up