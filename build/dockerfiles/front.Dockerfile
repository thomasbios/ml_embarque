FROM node:17

WORKDIR /usr/src/app

COPY front/package.json package.json
RUN npm install

COPY front/ .

EXPOSE 8000
CMD [ "node", "index.js" ]
