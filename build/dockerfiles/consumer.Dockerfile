FROM datamechanics/spark:3.2-latest

ENV PYSPARK_MAYOR_PYTHON_VERSION=3

WORKDIR /opt/app/

COPY requirements-consumer.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY src/consumer.py consumer.py

CMD [ "spark-submit", "--packages", "org.apache.spark:spark-sql-kafka-0-10_2.12:3.2.0", "consumer.py" ]
