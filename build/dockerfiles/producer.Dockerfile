FROM python:3.9

WORKDIR /opt/app/

COPY requirements-producer.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY src/producer.py producer.py

CMD [ "python3", "producer.py" ]