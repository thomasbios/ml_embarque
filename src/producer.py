from kafka import KafkaProducer
from datetime import datetime
import uuid
import json
from faker import Faker
from faker.providers import geo
import time
import random

faker_obj = Faker()
faker_obj.add_provider(geo)


class GenerateFakeData:
    def __init__(self):
        self.date_time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        self.uuid = uuid.uuid4().hex
        self.person_name = faker_obj.name()
        self.location = faker_obj.location_on_land()

    def get_json(self) -> str:
        return json.dumps(self.__dict__)


class Producer:
    def __init__(self, ip_port: str, topic: str):
        self._ip_port = ip_port
        self._topic = topic
        self._kafka_producer = KafkaProducer(bootstrap_servers=self._ip_port)

    def send_report(self, report: GenerateFakeData()):
        self._kafka_producer.send(
            self._topic, bytes(report.get_json(),
                               encoding="raw_unicode_escape"))
        print(f"Report sent: uuid: {report.uuid}")


if __name__ == "__main__":
    prod = Producer("localhost:29092", "camera_datas")
    i = 0
    while 1:
        test_report = GenerateFakeData()
        prod.send_report(test_report)
        print(f"Report {i} sent")
        i += 1
        time.sleep(random.randint(3, 10))
