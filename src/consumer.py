from kafka import KafkaConsumer
from pyspark.sql import SparkSession
import time
from pyspark.sql.dataframe import DataFrame


class Consumer:
    def __init__(self, ip_port: str, topic: str):
        self._ip_port = ip_port
        self._topic = topic
        self.spark = SparkSession \
        .builder \
        .master("local[*]") \
        .getOrCreate()

        self.spark.sparkContext.setLogLevel('WARN')

    def funcBatch(self, data: DataFrame, batchId: int):
        data.printSchema()
        data.show(truncate=False)

    def read_all(self):
        while 1:
            df = self.spark.readStream \
                .format("kafka") \
                .option( \
                    "kafka.bootstrap.servers",
                    self._ip_port
                ) \
                .option("subscribe", self._topic) \
                .option("startingOffsets", "latest") \
                .load() \
                .selectExpr("CAST(value AS STRING)")

            df.writeStream.foreachBatch(self.funcBatch).outputMode(
                "append").start().awaitTermination()
            time.sleep(10)


if __name__ == "__main__":
    consumer = Consumer("kafka:9092", "camera_datas")
    consumer.read_all()